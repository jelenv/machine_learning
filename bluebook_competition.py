#######################################################
#                                                     #
# Blue Book for Bulldozers Kaggle Competition         #
#                                                     #
# Using several regressors with stacking              #
#                                                     #
# Author: Vid Jelen                                   #
#                                                     #
#######################################################

import numpy as np
import pandas as pd
import random
import os
import cPickle as pickle
import time
import Orange
from Orange.regression import earth
from metrics import rmsle
from dateutil.parser import parse
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.ensemble import ExtraTreesClassifier
from sklearn import tree
from sklearn import *
from feature_parsing import *

def get_date_dataframe(date_column):
    return pd.DataFrame({
        "SaleYear": [d.year for d in date_column],
        "SaleMonth": [d.month for d in date_column],
        "SaleDay": [d.day for d in date_column]
        }, index=date_column.index)

def train_predict(train_sample, train_targets, valid_sample, regressor):
    regressor.fit(train_sample, train_targets)
    return regressor.predict(valid_sample)

def convert_dataframe_to_orange(df):
    df.to_csv('_temp_.csv', index_label='SalesID')
    orange_table = Orange.data.Table('_temp_.csv')
    os.unlink('_temp_.csv')
    return orange_table

# Output file with final predictions
out_file = "Stacking_kaggle.csv"
pickle_file = "bluebook_data.pkl"

"""Used for timing purposes"""
start = time.time()

"""Speed up data preparation with pickling"""
if not os.path.exists(pickle_file):

    training_set = "Train.csv"        # File with the training dataset
    test_set = "Valid.csv"            # File with the testing dataset

    train = pd.read_csv(training_set, converters={"saledate": parse})
    test = pd.read_csv(test_set, converters={"saledate": parse})

    ###############################################################################
    """Binarize values... e.g. [3,2,1,0] into [0,0,1], [0,1,0], [1,0,0], [0,0,0]"""
    ###############################################################################
    binarized_columns = [parse_year(train["YearMade"]), parse_auctioneerID(train["auctioneerID"]), parse_usageband(train["UsageBand"])]

    columns = set(train.columns)
    columns.remove("SalesID")
    columns.remove("SalePrice")
    columns.remove("saledate")

    #######################################
    """Binarize some additional features"""
    #######################################
    columns.remove("YearMade")
    columns.remove("auctioneerID")
    columns.remove("UsageBand")

    train_fea = get_date_dataframe(train["saledate"])
    test_fea = get_date_dataframe(test["saledate"])

    #############################################################
    """Put the parsed binarized values back into the DataFrame"""
    #############################################################
    train_fea = train_fea.join(binarized_columns)
    test_fea = test_fea.join(binarized_columns)

    # Map consecutive integers onto strings and fill the empty values
    for col in columns:
        if train[col].dtype == np.dtype('object'):
            s = np.unique(train[col].fillna(0).values)
            mapping = pd.Series([x[0] for x in enumerate(s)], index = s)
            train_fea = train_fea.join(train[col].map(mapping).fillna(0))
            test_fea = test_fea.join(test[col].map(mapping).fillna(0))
        else:
            train_fea = train_fea.join(train[col].fillna(0))
            test_fea = test_fea.join(test[col].fillna(0))

    pickle.dump((train, train_fea, test_fea), open(pickle_file, "wb"), -1)

else:
    train, test, train_fea, test_fea = pickle.load(open(pickle_file, "rb"))

# This actually makes the predictions worse... not worth using
"""Tree-based Feature selection"""
#fs = ExtraTreesClassifier(compute_importances=True, random_state=0)
#fs.fit(train_fea, train["SalePrice"])
#train_fea = pd.DataFrame(fs.transform(train_fea))
#test_fea = pd.DataFrame(fs.transform(test_fea))

"""Random sampling for 25% and 75% populations from the training set"""
random.seed(14)
np.random.seed(14)
rows = random.sample(train_fea.index, len(train_fea)/4)
valid_sample = train_fea.ix[rows]
valid_targets = train["SalePrice"][rows]
train_sample = train_fea.drop(rows)
train_targets = train["SalePrice"].drop(rows)

del rows    # for saving memory

#----------------------------------#
#-----------Stacking---------------#
#----------------------------------#

# Instantiate and fit the Regressors to the 75% of the training data, then do predictions on the 25% of the held-out (validation) training data.
"""Linear Regression"""
regr = linear_model.LinearRegression()
lr_predictions = train_predict(train_sample, train_targets, valid_sample, regr)
lr_predictions[lr_predictions < 0] = np.mean(lr_predictions[lr_predictions > 0]) # Set all the negative predictions to the mean prediction  (tried 0 and min and both gave worse results)

"""Bayesian Ridge Regression"""
brr = linear_model.BayesianRidge(n_iter=100, tol=0.001, alpha_1=1e-1, alpha_2=1e-1, lambda_1=1e-6, lambda_2=100)
brr_predictions = train_predict(train_sample, train_targets, valid_sample, brr)
brr_predictions[brr_predictions < 0] = np.mean(brr_predictions[brr_predictions > 0]) # Set all the negative predictions to the mean prediction

"""Lasso Regression"""
lass = linear_model.Lasso(alpha = 100, max_iter=1700, tol=0.25)
lass_predictions = train_predict(train_sample, train_targets, valid_sample, lass)
lass_predictions[lass_predictions < 0] = np.mean(lass_predictions[lass_predictions > 0]) # Set all the negative predictions to the mean prediction

"""K-nearest Neighbors Regression"""
knn = neighbors.KNeighborsRegressor(n_neighbors = 10, weights = "distance", leaf_size=30, p=1)
knn_predictions = train_predict(train_sample, train_targets, valid_sample, knn)

"""Decision Tree Regression"""
dt = tree.DecisionTreeRegressor(max_depth=8, min_samples_split=3, min_samples_leaf=6)
dt_predictions = train_predict(train_sample, train_targets, valid_sample, dt)

"""Gradient Boosting Regressor"""
gbr = GradientBoostingRegressor()
gbr_predictions = train_predict(train_sample, train_targets, valid_sample, gbr)
gbr_predictions[gbr_predictions < 0] = np.mean(gbr_predictions[gbr_predictions > 0]) # Set all the negative predictions to the mean prediction

"""Multivariate Adaptive Regression Splines - MARS"""
orange_sample = train_sample.join(train_targets)
orange_data = convert_dataframe_to_orange(orange_sample)
mars_regr = earth.EarthLearner(orange_data, terms=30, degree=2, penalty=1.0)
orange_target = valid_sample.join(valid_targets)
orange_ending = convert_dataframe_to_orange(orange_target)
mars_predictions = np.array([mars_regr(x).value for x in orange_ending])
mars_predictions[mars_predictions < 0] = np.mean(mars_predictions[mars_predictions > 0]) # Set all the negative predictions to the mean prediction

del orange_sample   # for saving memory
del orange_data     # for saving memory
del orange_target   # for saving memory
del orange_ending   # for saving memory

# SVM takes too much time to train
#"""Support Vector Machine Regression"""
#svr = svm.SVR(kernel='linear', cache_size=4000)
#svr.fit(train_sample, train_targets)
#svr_predictions = svr.predict(valid_sample)

"""Random Forests Regressor"""
rf = RandomForestRegressor(n_estimators=50 ,n_jobs=-1)
rf_predictions = train_predict(train_sample, train_targets, valid_sample, rf)

"""Determine the root mean square log error of the predictions"""
error = rmsle(valid_targets, lr_predictions)
print("Linear regression RMSLE: %f" % error)
error = rmsle(valid_targets, brr_predictions)
print("Bayesian Ridge regression RMSLE: %f" % error)
error = rmsle(valid_targets, lass_predictions)
print("Lasso regression RMSLE: %f" % error)
error = rmsle(valid_targets, knn_predictions)
print("K-nearest Neighbors regression RMSLE: %f" % error)
error = rmsle(valid_targets, dt_predictions)
print("Decision Tree regression RMSLE: %f" % error)
error = rmsle(valid_targets, gbr_predictions)
print("Gradient Boosting regression RMSLE: %f" % error)
error = rmsle(valid_targets, mars_predictions)
print("Multivariate Adaptive Regression Splines RMSLE: %f" % error)
error = rmsle(valid_targets, rf_predictions)
print("Random Forests regression RMSLE: %f" % error)

del error # for saving memory

# Create the level-1 dataset from level-0 predictions
level_1_data = np.array([lr_predictions, brr_predictions,lass_predictions, knn_predictions, dt_predictions, gbr_predictions, mars_predictions, rf_predictions]).transpose()

# Create a level-1 stacking generalizer
l1_rf = RandomForestRegressor(n_estimators=50, n_jobs=-1)
l1_rf.fit(level_1_data, valid_targets)

del level_1_data    # for saving memory
del train_sample    # for saving memory
del train_targets   # for saving memory
del valid_sample    # for saving memory
del valid_targets   # for saving memory

mars_ending = convert_dataframe_to_orange(test_fea)

# Fit the regressors to the 100% of the training dataset
regr.fit(train_fea, train["SalePrice"])
brr.fit(train_fea, train["SalePrice"])
lass.fit(train_fea, train["SalePrice"])
knn.fit(train_fea, train["SalePrice"])
dt.fit(train_fea, train["SalePrice"])
gbr.fit(train_fea, train["SalePrice"])
rf.fit(train_fea, train["SalePrice"])
orange_sample = train_fea.join(train["SalePrice"])
orange_data = convert_dataframe_to_orange(orange_sample)
mars_regr = earth.EarthLearner(orange_data, terms=30, degree=2, penalty=1.0)

del train # for saving memory

# Now use the regressors on the test dataset
final_mars = np.array([mars_regr(x).value for x in mars_ending])
final_regr = regr.predict(test_fea)
final_brr = brr.predict(test_fea)
final_lass = lass.predict(test_fea)
final_knn = knn.predict(test_fea)
final_dt = dt.predict(test_fea)
final_gbr = gbr.predict(test_fea)
final_rf = rf.predict(test_fea)
final_gbr[final_gbr < 0] = np.mean(final_gbr[final_gbr > 0])        # Set all the negative predictions to the mean prediction
final_regr[final_regr < 0] = np.mean(final_regr[final_regr > 0])    # Set all the negative predictions to the mean prediction
final_brr[final_brr < 0] = np.mean(final_brr[final_brr > 0])        # Set all the negative predictions to the mean prediction
final_lass[final_lass < 0] = np.mean(final_lass[final_lass > 0])    # Set all the negative predictions to the mean prediction
final_mars[final_mars < 0] = np.mean(final_mars[final_mars > 0])    # Set all the negative predictions to the mean prediction

predictions = np.array([final_regr, final_brr, final_lass, final_knn, final_dt, final_gbr, final_mars, final_rf]).transpose()

# Use the level-1 stacking generalizer on the test data predictions to make final predictions
final_predictions = l1_rf.predict(predictions)

test = test.join(pd.DataFrame({"SalePrice": final_predictions}))
test.ix[test.SalePrice < 0, "SalePrice"] = np.mean(test.ix[test.SalePrice > 0, "SalePrice"]) #Set all the negative predictions to the mean prediction
test[["SalesID", "SalePrice"]].to_csv(out_file, index=False)

"""Used for timing purposes"""
print("\nSeconds elapsed: {}".format(time.time() - start))

