import pandas as pd

"""Functions for binarizing additional discrete features"""

def parse_auctioneerID(aucID_column):
    return pd.DataFrame({
        "First_auc": [1 if x == 3 else 0 for x in aucID_column],
        "Second_auc": [1 if x == 1 else 0 for x in aucID_column],
        "Third_auc": [1 if x == 2 else 0 for x in aucID_column],
        "Fourth_auc": [1 if x == 4 else 0 for x in aucID_column],
        "Fifth_auc": [1 if x == 6 else 0 for x in aucID_column],
        "Rest_auc": [0 if x == 3 or x == 1 or x == 2 or x == 4 or x == 6 else 1 for x in aucID_column]
        }, index=aucID_column.index)

def parse_datasource(datasource_column):
    return pd.DataFrame({
        "First_datasource": [1 if x == 121 else 0 for x in datasource_column],
        "Second_datasource": [1 if x == 132 else 0 for x in datasource_column],
        "Third_datasource": [1 if x == 136 else 0 for x in datasource_column],
        "Fourth_datasource": [1 if x == 149 else 0 for x in datasource_column],
        "Fifth_datasource": [1 if x == 172 else 0 for x in datasource_column],
        "Other_datasource": [0 if x == 121 or x == 132 or x == 136 or x == 149 or x == 172 else 1 for x in datasource_column]
        }, index=datasource_column.index)

def parse_usageband(usageband_column):
    return pd.DataFrame({
        "Low_usage": [1 if x == "Low" else 0 for x in usageband_column],
        "Medium_usage": [1 if x == "Medium" else 0 for x in usageband_column],
        "High_usage": [1 if x == "High" else 0 for x in usageband_column],
        "Unknown_usage": [0 if x == "Low" or x == "Medium" or x == "High" else 1 for x in usageband_column]
        }, index=usageband_column.index)

def parse_product_size(product_size_column):
    return pd.DataFrame({
        "Compact_size": [1 if x == "Compact" else 0 for x in product_size_column],
        "Mini_size": [1 if x == "Mini" else 0 for x in product_size_column],
        "Small_size": [1 if x == "Small" else 0 for x in product_size_column],
        "Medium_size": [1 if x == "Medium" else 0 for x in product_size_column],
        "Large_size": [1 if x == "Large" else 0 for x in product_size_column],
        "Undefined_size": [0 if x == "Compact" or x == "Mini" or x == "Medium" or x == "Large" else 1 for x in product_size_column]
        }, index=product_size_column.index)

def parse_product_group(product_group_column):
    return pd.DataFrame({
        "Backhoe_Loaders_group": [1 if x == "Backhoe Loaders" else 0 for x in product_group_column],
        "Motor_Graders_group": [1 if x == "Motor Graders" else 0 for x in product_group_column],
        "Skid_Steer_Loaders_group": [1 if x == "Skid Steer Loaders" else 0 for x in product_group_column],
        "Track_excavators_group": [1 if x == "Track Excavators" else 0 for x in product_group_column],
        "Track_Type_Tractors_group": [1 if x == "Track Type Tractors" else 0 for x in product_group_column],
        "Wheel_Loader_group": [1 if x == "Wheel Loader" else 0 for x in product_group_column]
        }, index=product_group_column.index)

def parse_drive_system(drive_system_column):
    return pd.DataFrame({
        "All_wheel_drive": [1 if x == "All Wheel Drive" else 0 for x in drive_system_column],
        "Four_wheel_drive": [1 if x == "Four Wheel Drive" else 0 for x in drive_system_column],
        "Two_wheel_drive": [1 if x == "Two Wheel Drive" else 0 for x in drive_system_column],
        "Undefined_drive_system": [0 if x == "All Wheel Drive" or x == "Four Wheel Drive" or x == "Two Wheel Drive" else 1 for x in drive_system_column]
        }, index=drive_system_column.index)

def parse_enclosure(enclosure_column):
    return pd.DataFrame({
        "Erops_group": [1 if x == "EROPS" else 0 for x in enclosure_column],
        "Erops_ac_group": [1 if x == "EROPS AC" else 0 for x in enclosure_column],
        "Erops_w_ac_group": [1 if x == "EROPS w AC" else 0 for x in enclosure_column],
        "No_rops_group": [1 if x == "NO ROPS" else 0 for x in enclosure_column],
        "Orops_group": [1 if x == "OROPS" else 0 for x in enclosure_column],
        "Undefined_enclosure": [0 if x == "EROPS" or x == "EROPS AC" or x == "EROPS w AC" or x == "NO ROPS" or x == "OROPS" else 1 for x in enclosure_column]
        }, index=enclosure_column.index)